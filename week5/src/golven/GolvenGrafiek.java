package golven;

import java.awt.*;
import java.util.Random;

public class GolvenGrafiek {
    private int aantal;
    private Random random;

    public GolvenGrafiek(int aantal) {
        this.aantal = aantal;
        random = new Random();
    }

    public void tekenGolven(){
        GrafiekWindow gw = new GrafiekWindow(10, 6);
        for (int i = 0; i <= aantal; i++) {
            Golf golf = new Golf();
            golf.setAmplitude(4.0 * random.nextDouble());
            golf.setFrequentie(4.0 * random.nextDouble());
            float r = random.nextFloat();
            float g = random.nextFloat();
            float b = random.nextFloat();
            for (double j = -5; j <= 5.0; j+=0.0001) {
                gw.tekenPunt(j, golf.getYWaarde(j), new Color(r, g, b));
            }
        }
        gw.toon();
    }
}
