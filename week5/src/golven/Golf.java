package golven;

public class Golf {
    private double amplitude;
    private double frequentie;

    public Golf() {
    }

    public void setAmplitude(double amplitude) {
        if (amplitude != 0){
            this.amplitude = amplitude;
        }
    }

    public void setFrequentie(double frequentie) {
        if (frequentie != 0) {
            this.frequentie = frequentie;
        }
    }

     public double getYWaarde(double x){
         return (amplitude * Math.sin(frequentie * x));
     }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("y = ");
        if (amplitude != 1){
            sb.append(String.format("%.1f ", amplitude));
        }
        sb.append("sin (");
        if (frequentie != 1){
            sb.append(String.format("%.1f ", frequentie));
        }
        sb.append("x)");
        return sb.toString();
    }
}
