package golven;

public class GolfTest {
    public static void main(String[] args) {
        for(double i = 1; i <= 5; i+=0.5){
            Golf g = new Golf();
            g.setFrequentie(2);
            g.setAmplitude(i);
            System.out.println(g);
        }
        GolvenGrafiek gg = new GolvenGrafiek(5);
        gg.tekenGolven();
    }

}
