import java.util.Scanner;

/**
 * @author Anouk As
 * @version 1.0 10/10/2019 17:15
 */
public class Galgje1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String woord;
        do {
            System.out.println("Geef een woord (max 10 letters)");
            woord = keyboard.nextLine();
        } while (woord.length() < 5 || woord.length() > 10);
        for (int i = 0; i < 20; i++) {
            System.out.println();
        }
        System.out.print("Het te zoeken woord: ");
        for (int i = 0; i < woord.length(); i++) {
            System.out.print(".");
        }
        System.out.println();
        boolean geraden = false;
        int i = 0;
        do{
            i++;
            System.out.println("Doe een gok: ");
            String gok = keyboard.nextLine();
            if(gok.toUpperCase().equals(woord.toUpperCase())){
                geraden = true;
            }
            else {
                System.out.print("Fout! ");
            }
        }while (!geraden || i <= 5);
        if(geraden){
            System.out.println("Proficiat je hebt het woord geraden in " + i + " beurten! ");
        }

    }
}
