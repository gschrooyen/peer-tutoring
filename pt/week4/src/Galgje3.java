import java.util.Scanner;

/**
 * @author Anouk As
 * @version 1.0 10/10/2019 17:36
 */
public class Galgje3 {
    public static void main(String[] args) {
        final String HANGMAN =
                "----- \n" +
                        "| | \n" +
                        "| O \n" +
                        "| /|\\\n" +
                        "| | \n" +
                        "| / \\\n" +
                        "| \n" +
                        "-------";

        Scanner keyboard = new Scanner(System.in);
        String woord;
        do {
            System.out.println("Geef een woord (max 10 letters)");
            woord = keyboard.nextLine();
        } while (woord.length() < 5 || woord.length() > 10);
        for (int i = 0; i < 20; i++) {
            System.out.println();
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < woord.length(); i++) {
            stringBuilder.append(".");
        }
        System.out.println("Het te zoeken woord: " + stringBuilder);
        boolean geraden = false;
        int i = 0;
        do{
            i++;
            System.out.println("Raad een letter: ");
            char gok = keyboard.next().charAt(0);
            for (int j = 0; j < stringBuilder.length(); j++) {
                if(stringBuilder.charAt(j) == gok){
                    stringBuilder.setCharAt(j, gok);
                }
            }
            System.out.println("Het te zoeken woord: " + stringBuilder);
            System.out.println(HANGMAN.substring(0, 7*i));
            if(stringBuilder.toString().equals(woord)){
                geraden = true;
            }
        }while (!geraden || i <= 8);
        if(geraden){
            System.out.println("Proficiat je hebt het woord geraden in " + i + " beurten! ");
        }
    }
}
