import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private final static double MIN_DONATIE = 0.45;

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Heb je de laatste 4 maanden een tatoeage laten zetten? (J/N): ");
        char tatoe = keyboard.nextLine().charAt(0);
        if(tatoe == 'J'){
            System.out.println("Jammer, je komt niet in aanmerking om bloed te geven");
        }

        char geslacht;
        do {
            System.out.print("Wat is uw geslacht (M/V): ");
            geslacht = keyboard.nextLine().charAt(0);
            if(geslacht == 'M'){
                System.out.print("Heb je seksuele betrekkeningen gehad met een andere man? (J/N): ");
                char antwoord = keyboard.nextLine().charAt(0);
                if(antwoord == 'J'){
                    System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                    System.exit(1);
                }
            }

            if(geslacht == 'V') {
                System.out.print("Bent u zwanger? (J/N): ");
                char antwoord = keyboard.nextLine().charAt(0);
                if(antwoord == 'J'){
                    System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                    System.exit(1);
                }
            }
        }while (geslacht != 'M' && geslacht != 'V');

        System.out.println("Wat is uw leeftijd: ");
        int leeftijd = keyboard.nextInt();
        if(leeftijd < 18){
            System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
            System.exit(1);
        }
        else if(leeftijd > 66){
            System.out.println("Hebt u al eerder bloed gegeven? (J/N)");
            char antwoord = keyboard.nextLine().charAt(0);
            if( antwoord == 'N'){
                System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                System.exit(1);
            }
            if(antwoord == 'J' && leeftijd <= 71){
                System.out.println("Is uw laatste bloedgifte van minder dan 3 jaar geleden? (J/N)");
                char antwoord2 = keyboard.nextLine().charAt(0);
                if(antwoord2 == 'N'){
                    System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                    System.exit(1);
                }
            }
            else {
                System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
                System.exit(1);
            }
        }

        System.out.println("Wat is uw lengte? (in meter)");
        double lengte = keyboard.nextDouble();
        System.out.println("Wat is uw gewicht? (in kg)");
        double gewicht = keyboard.nextDouble();
        double bloedvolume = 0;
        if(geslacht == 'M'){
            bloedvolume = (0.3669*Math.pow(lengte, 3)) + (0.03219*gewicht + 0.6041);
        }
        else if(geslacht == 'V'){
            bloedvolume = (0.3561*Math.pow(lengte, 3)) + (0.03308*gewicht + 0.1833);
        }
        System.out.println("Bloedvolume: " + bloedvolume);
        double maxDonatie = bloedvolume*0.13;
        System.out.println("Max donatie:" + maxDonatie);
        System.out.println("Min donatie: " + MIN_DONATIE);
        boolean magBloedGeven = false;
        if (maxDonatie > MIN_DONATIE){
            magBloedGeven = true;
            System.out.println("Je mag WEL bloed geven");
        }else {
            System.out.println("Je mag GEEN bloed geven");
        }

        //DEEL 2: GRAFIEK
        System.out.printf("Bloedvolume: ");
        for (double i = 0; i < bloedvolume; i = i + 0.1) {
            System.out.printf("*");
        }
        System.out.println();
        System.out.printf("Max donatie: ");
        for (double i = 0; i < maxDonatie;  i = i + 0.1) {
            System.out.printf("*");
        }
        System.out.println();
        System.out.printf("Min donatie: : ");
        for (double i = 0; i < MIN_DONATIE;  i = i + 0.1) {
            System.out.printf("*");
        }
    }
}
