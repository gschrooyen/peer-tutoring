package com.kdg.be;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("maak een keuze: ");
        System.out.println("\t1) oefening 1");
        System.out.println("\t2) oefening 2");
        String input = keyboard.nextLine();
        switch (input) {
            case "1":
                oefening1();
                break;
            case "2":
                oefening2();
                break;
            default:
                System.exit(-1);
        }
    }

    private static void oefening2() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("conversietabel °C naar °F");
        System.out.println("===========================");
        System.out.println("geef de begintemperatuur in °C:");
        int begin = keyboard.nextInt();
        System.out.println("geef de eindtemperatuur in °C:");
        int eind = keyboard.nextInt();
        if (begin > eind){
            System.out.println("beginwaarde moet kleiner zijn dan eindwaarde");
            System.exit(-1);
        }
        System.out.println("geef de stapwaarde");
        int stap = keyboard.nextInt();
        System.out.printf("%S\n", "==================");
        System.out.printf("|   %S  |   %S   |\n", "°C", "°F");
        System.out.printf("%S\n", "|----------------|");
        for (int i = begin; i <= eind; i += stap) {
            double uitkomst = (i*1.8)+32;
            System.out.printf("|%5d  |  %.1f  |\n", i, uitkomst);
        }
        System.out.printf("%S\n", "==================");
        System.exit(0);
    }

    private static void oefening1(){
        String keuze = "";
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Conversie graden Celsius - Farenheid");
        System.out.println("===================================");

        while (!keuze.equals("0")){
            System.out.println("welke conversie wens je te doen?");
            System.out.println("\t1) °C naar °F");
            System.out.println("\t2) °F naar °C");
            System.out.print("uw keuze? ");
            keuze = keyboard.nextLine();
            System.out.println(keuze);
            if (keuze.equals("1")){
                System.out.print("geef uw waarde in °C: ");
                try {
                    int graden = keyboard.nextInt();
                    double uitkomst = (graden * 1.8) + 32;
                    System.out.println(graden+"°C = " + uitkomst +"°F");
                    keyboard.nextLine();
                }catch (InputMismatchException e){
                    System.out.println("Dat was geen getal!!");
                }
            }else if (keuze.equals("2")){
                System.out.print("geef uw waarde in °F: ");
                try {
                    int graden = keyboard.nextInt();
                    double uitkomst = (graden -32) / 1.8;
                    System.out.println(graden+"°F = "+uitkomst+"°C");
                    keyboard.nextLine();
                }catch (InputMismatchException e){
                    System.out.println("dat was geen getal!!");
                }
            }else if (!keuze.equals("0")){
                System.out.println("gelieve een juiste keuze te maken "+ keuze+ " is geen optie.");
            }else if (keuze.equals("0")){
                System.out.println("tot zines!");
            }
        }

        System.exit(0);
    }
}
