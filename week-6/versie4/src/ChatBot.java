import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class ChatBot {
    private String naam;
    private Random random;
    private String[] antwoorden;
    private String[] keywords;
    private String[] keywordAwnsers;
    private static HashMap<String, Integer> aantal = new HashMap<>();
    private static HashMap<String, Integer> factuur = new HashMap<>();
    private long begintijd;
    private String[] scheldwoorden;

    public ChatBot(String naam) {
        this.naam = naam;
        random = new Random();
        antwoorden = new String[]{"Ok, probeer eerst al eens te herstarten",
                "Kan je het probleem eens herformuleren?",
                "En is dat al lang zo?",
                "Waarom heb je ons niet eerder gecontacteerd?",
                "Ik denk dat je dat zelf wel kan oplossen, niet?",
                "Die vraag heb ik nog niet vaak gehad!",
                "Sorry, ik was even bezig, kan je de vraag eens herhalen?",
                "Aha, gekend probleem, ik zoek het op en laat je iets weten.",
                "Sorry, daar heb ik niet direct een antwoord op. Wat nu?",
                "En dan?",
                "Dat zal wel, maar er zijn ergere dingen he.",
                "Hmm, die moet ik opzoeken. En wanneer deed dat zich juist voor?"};
        keywords = new String[]{"aanloggen", "traag", "scherm", "hangt", "kleuren", "trager", "geluid",
                "vooruit", "niet", "gisteren", "weekend", "geen idee", "oplossing",
                "geen", "zeker"};
        keywordAwnsers = new String[]{"Probeer opnieuw aan te loggen, lost dat je probleem op?",
                "Herstarten van het systeem kan vele snelheidsproblemen oplossen. Heb je dat al geprobeerd?",
                "Misschien is er iets mis met de video adapter?",
                "Je kan best nog even wachten, mogelijk lost het probleem zichzelf op.",
                "Heb je onlangs nieuwe videosoftware geïnstalleerd?",
                "Is er nog voldoende ruimte vrij op het toestel?",
                "Het volume staat toch juist he?",
                "Mogelijk een probleem met één van de systeemdrivers.",
                "Pas op, soms wel hoor.",
                "En eergisteren?",
                "In de week werken die dingen meestal vlotter...",
                "Niet wanhopen, we vinden samen wel een oplossing.",
                "Ik zal het wel oplossen, geef jij mij gewoon wat meer info.",
                "Echt geen?",
                "Ik ken dat 'zeker', uiteindelijk blijkt het toch niet!"};
        begintijd = System.currentTimeMillis() + 60000;
        scheldwoorden = new String[]{"kanker", "kut", "fuck", "tering"};

    }

    @Override
    public String toString() {
        return "hallo ik ben chatbot " + naam + ", stel me een vraag en ik geef je een antwoord";
    }

    public String antwoordOpVraag(String vraag) {
        aantal.put(naam, aantal.getOrDefault(naam, 0) +1);
        if (vraag.toLowerCase().equals("stop")) {
            aantal.remove(naam);
            factuur.remove(naam);
            return "Je hebt blijkbaar geen vragen meer. Ok, dan ben ik weg. De groeten van " + naam + "\n\n"+ getFactuur();
        } else if (vraag.length() < 4) {
            return "";
        }
        for (String s : scheldwoorden) {
            if (vraag.contains(s)){
                factuur.put(naam, factuur.getOrDefault(naam, 0) + 5);
                return "gelieve beleefd te blijven";
            }
        }
        String antwoord = "";
        if (aantal.get(naam) > 5){
            antwoord = "Zucht, typisch. ";
        }

        for (String k :
                keywords) {
            if (vraag.contains(k)) {
                return antwoord + keywordAwnsers[Arrays.asList(keywords).indexOf(k)];
            }
        }
        return antwoord + antwoorden[random.nextInt(antwoorden.length)];
    }

    private String getFactuur(){
        int start = factuur.getOrDefault(naam, 0);
        long tijdbedrag = 0;
        if (begintijd <= System.currentTimeMillis()){
            long tijd = System.currentTimeMillis() - begintijd;
            tijdbedrag = tijd / 15000;
        }
        long eindbedrag = start + tijdbedrag + 10;
        return "Factuur:\n" +
                "Item               Prijs\n" +
                "==========================\n" +
                "Opstart:           € 10\n" +
                "Extra time:\n" +
                String.format("%dx15seconden       € %d\n", tijdbedrag, tijdbedrag)+
                "scheldboete        € "+start+"\n"+
                "TOTAAL             € "+eindbedrag;
    }
}
