package wijnen;

import java.time.LocalDateTime;

/**
 * PEER opdracht
 * P2W2
 */
public class WijnHuis {
    private static final int MAX_AANTAL = 10;
    private Wijn[] wijnen = new Wijn[MAX_AANTAL];  //voorlopig gevuld met 10 null-objecten
    private String naam;
    private int aantal = 0;

    public WijnHuis(String naam) {
        this.naam = naam;
    }

    public void voegWijnToe(Wijn wijn) {
        if (aantal < MAX_AANTAL){
            if (!zoekWijn(wijn)){
                wijnen[aantal] = wijn;
                ++aantal;
            }
        }
    }

    public boolean zoekWijn(Wijn wijn) {

            for (int i = 0; i < aantal; i++){
                if (wijnen[i].getNaam().equals(wijn.getNaam())){
                    return true;
                }
            }

        return false;
    }

    public Wijn getOudsteWijn() {
        if (aantal != 0){
            Wijn oudste = wijnen[0];
            for (Wijn wijn :
                    wijnen) {
                if (wijn.getOogstDatum().isBefore(oudste.getOogstDatum())){
                    oudste = wijn;
                }
            }
            return oudste;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("Wijnhuis %s\n", naam.toUpperCase()));

        StringBuilder wijnenTekst = new StringBuilder();
        StringBuilder champagneTekst = new StringBuilder();
        StringBuilder likeurenTekst = new StringBuilder();
        for (int i = 0; i < aantal; i++){
            if (wijnen[i] instanceof Champagne){
                champagneTekst.append(wijnen[i]);
            }else if (wijnen[i] instanceof Likeur){
                likeurenTekst.append(wijnen[i]);
            }else{
                wijnenTekst.append(wijnen[i]);
            }
        }
        return result.toString();
    }
}
