package wijnen;

import java.time.LocalDate;

/**
 * PEER opdracht
 * P2W2
 */
public class Wijn {
    private String naam;
    private String streek;
    private LocalDate oogstDatum;
    private double basisPrijs;

    public Wijn(String naam, String streek, LocalDate oogstDatum, double basisPrijs) {
        this.naam = naam;
        this.streek = streek;
        this.oogstDatum = oogstDatum;
        this.basisPrijs = basisPrijs;
    }

    public String getNaam() {
        return naam;
    }

    public String getStreek() {
        return streek;
    }

    public LocalDate getOogstDatum() {
        return oogstDatum;
    }

    double getBasisPrijs() {
        return basisPrijs;
    }

    public double berekenPrijs() {
        return getBasisPrijs();
    }

    protected String getKenmerken() {
        return String.format("Van %d, afkomstig uit %s", oogstDatum.getYear(), streek);
    }

    @Override
    public String toString() {
        return String.format("%-45s€%6.2f\n\t(%s)", getNaam(), berekenPrijs(), getKenmerken());
    }
}
